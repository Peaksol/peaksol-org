# Contact

I can be reached in the following ways:

- E-mail: `p AT peaksol DOT org` (outgoing address may be different);
- Matrix: `@peaksol:frcsm.de`;
- XMPP: `peaksol@tilde.team`.

You may also find me on:

- [Fediverse/Mastodon](https://floss.social/@peaksol);
- [Codeberg](https://codeberg.org/Peaksol).

## OpenPGP

Use of OpenPGP email encryption is encouraged. A copy of my public key can be acquired on [OpenPGP keyserver], [Ubuntu keyserver] or [this website].

My fingerprint is `6D80 65CB 9EC5 7633 8E87 FD91 EB83 7FB5 327E 854F`. Please also check the fingerprint with me elsewhere before trusting my keys.

[OpenPGP keyserver]:https://keys.openpgp.org/vks/v1/by-fingerprint/6D8065CB9EC576338E87FD91EB837FB5327E854F
[Ubuntu keyserver]:https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x6d8065cb9ec576338e87fd91eb837fb5327e854f
[this website]:peaksol.asc

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
