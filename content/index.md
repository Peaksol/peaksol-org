---
{
	"title": "Peaksol's Personal Site",
	"style": "home.css",
	"nav": [
		{
			"title": "Contact",
			"link": "contact.html"
		},
		{
			"title": "Posts",
			"link": "posts/"
		}
	]
}
---

<div class="standout">

## About me

I am Wu Rui, also known as "Peaksol," a random tech enthusiast. I advocate [software freedom](https://learn.fsfans.club/index.en.html), maintain a few [projects](https://codeberg.org/Peaksol) of my own, and contribute translation to some community-driven projects. Beyond computing, I take passionate interest in linguistics, law and music.

</div>

---

[exoring] - [prev][exoring-prev] / [rand][exoring-rand] / [next][exoring-next]

[exoring]:https://ring.exozy.me/
[exoring-prev]:https://ring.exozy.me/previous?host=www.peaksol.org
[exoring-rand]:https://ring.exozy.me/random
[exoring-next]:https://ring.exozy.me/next?host=www.peaksol.org

[fediring] - [prev][fediring-prev] / [rand][fediring-rand] / [next][fediring-next]

[fediring]:https://fediring.net/
[fediring-prev]:https://fediring.net/previous?host=peaksol.org
[fediring-rand]:https://fediring.net/random
[fediring-next]:https://fediring.net/next?host=peaksol.org

This page is released into the public domain under [CC0](http://creativecommons.org/publicdomain/zero/1.0).
