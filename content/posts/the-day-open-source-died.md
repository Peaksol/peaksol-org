# 开源死亡之日：一段关于 Minecraft、Bukkit 和 GPL 的故事

作者：[Justin W. Flory](https://blog.jwf.io/2020/04/open-source-minecraft-bukkit-gpl/)  
译者：Peaksol

许久以前，在我才十多岁时，我曾自愿投身于 Minecraft 的开源圈子中。当时我是在一个叫 [Spigot](https://www.spigotmc.org/wiki/about-spigot/) 的项目中志愿做了一名工作人员。Spigot 是 Bukkit 项目的一个分支，如今已是最大的开源 Minecraft 服务端项目了。

这篇博文所讲述的故事，大致涵盖了从 2010 年到 2014 年间人们所理解的开源的意义、价值和前途。这段故事曾影响过无数的圈内人，而他们大多数都还是稚嫩的儿童、十几岁的少年和年青的成年人。这是一段关于 GNU General Public License (GPL)<sup>1</sup> 即成功了，又失败了的故事。

## 故事的起源：Bukkit、Minecraft 和 GPL

2010 年 11 月是故事的开始：**Bukkit** 诞生了。

> Bukkit 是一个新兴的 Minecraft 模组化服务端，它将彻底改变修改和运行 Minecraft 服务器的方式。有了它，管理和创建服务器将更加容易，也更加灵活。Bukkit 从其他 mod 所犯的错误中吸取了教训，立志要变得与众不同，并填补别人留下的空白：从头开始，我们便专注于性能、易用性、高度可定制性、以及你我（即团队与用户）的良好沟通。Bukkit 的整体设计受到了其他 mod 的启发，同时启发我们的还有我们同作为 Minecraft 玩家的经验。受此启发，我们便能在独特的视角和优势下创建 Bukkit 项目。
>
> — [About Us](https://web.archive.org/web/20141211115250/https://bukkit.org/pages/about-us/), Bukkit.org

Bukkit 是一个开源的 Minecraft 服务端，为开发者提供了 API 来制作插件，可以独特又趣味地扩展 Minecraft 的玩法。虽然 Bukkit 不是第一个开源的 Minecraft 服务端，但它是第一个有组织的项目。Bukkit 项目以 GNU General Public License (GPL) v3 启动。

从 2011 年到 2014 年，Bukkit 是开设 Minecraft 多人游戏服的标配。随着时间推移，Bukkit 服务端（及其衍生项目）的使用量已经超过了 Mojang 官方发布的服务端软件。Mojang 是负责开发 Minecraft 的公司。

## 辛勤工作受到青睐

这个项目的成功也受到了 Mojang 的认可。在 2011 年的第一次 Minecraft 大会——MINECON 上，Mojang 雇用了 Bukkit 的四名主开发者来参与 Minecraft 开发工作。然后，除了其中一人留下来了之外，其他三人都离开了 Bukkit。那名仍活跃在 Bukkit 的开发者又在 2013 年神秘地离开了 Mojang。

> 为 Mojang 工作的日子很愉快，但现在到了我追求其他兴趣的时候了。从昨天起，我不再为 Mojang 工作。
>
> — EvilSeph (@EvilSeph) [2013 年 10 月 2 日](https://twitter.com/EvilSeph/status/385537792794959872?ref_src=twsrc%5Etfw)

然而，有一点应该时刻注意。Bukkit 是根据 GPLv3 许可开源的项目。但是，它也对 Minecraft 的一部分游戏代码进行了逆向工程，以建立服务器代码和 API。这对 Bukkit 和 Mojang 来说，都不曾是问题：

> “我们在 2010 年 12 月创建 Bukkit 的时候，都不希望干出错事来。从一开始，我们就想确保我们给 Minecraft 带来的是积极的变化，是 Mojang 认可的变化。为此，我们和 Mojang 开了一场会议，来了解 Mojang 对我们项目的看法，并确保我们不会干出 Mojang 不喜欢的事。这场会议的关键在于 Mojang ‘喜欢我们在做的事’，而不是我们必须要做的事。可惜，我们都知道我们别无选择，便继续做下去了——只不过现在可以保证，我们的项目多半是不会在未来任何时候关闭了。”
>
> — EvilSeph (Warren Loo), "[Bukkit: The Next Chapter](https://web.archive.org/web/20150112163638/https://bukkit.org/threads/bukkit-the-next-chapter.62489/)"

Bukkit 通过逆向工程从 Minecraft 中得到了代码，但从来没有人提起过这个版权问题。然而，多年来，Bukkit 发布的 GPL 下的代码一直包含了官方 Minecraft 的代码。

## 第一幕：Minecraft 最终用户许可协议（EULA）

*《卫报》的这篇文章从另一个角度评价了 Minecraft EULA：[《Minecraft：规则的改变是如何撕裂社区的》](https://www.theguardian.com/technology/2014/jun/24/minecraft-how-a-change-to-the-rules-is-tearing-the-community-apart)。*

数年以来，一切都十分正常。尽管 Bukkit 的一些核心开发者都被 Mojang 雇用了，Bukkit 仍然是一个志愿者驱动的项目。然而，在 2014 年，Minecraft 圈子进入了一种奇怪的紧张局面。

这种紧张的局面的起因是 Minecraft 最终用户许可协议（EULA）的内容。EULA 在说到 Minecraft 多人游戏服务器的货币系统时，用到了含糊不清的语言：

> “一条重要规定是您不得分发我们创建的任何内容。‘分发我们创建的任何内容’是指‘向其他人提供我们游戏的副本、将我们创建的任何内容用于商业用途或赚钱，或允许其他人以不公平或不合理的方式访问我们创建的任何内容’。”
>
> — 2014: [account.mojang.com/documents/minecraft_eula](https://web.archive.org/web/20140706191831/https://account.mojang.com/documents/minecraft_eula)

随着许多开源项目围绕 Minecraft 圈子蓬勃发展，这个生态中也共存着游戏服务器的一大产业。多人游戏服务器的服主在运行 Bukkit （或 Spigot 之类的衍生项目）时，也创建了自己服务器的网页商店。玩家会在特定服务器用真钱购买游戏内的特权。在使用了开源插件的情况下，玩家会花钱购买物品礼包，其中就包含了钻石剑、虚拟金币，这些都是游戏内的消耗品。

数年来，这种行为都被任由其滋生。然而，在 2013 年 12 月，EULA 悄悄地被编辑了。但是，在 2014 年中期，圈内有人注意到了内容有改变，于是在 Twitter 上询问了 Mojang 员工：这是不是说，多人游戏服务器就不能再用真钱来出售游戏内的物品了？Mojang 员工用短短 140 个字尽可能地表达了意思：EULA 的内容确实禁止这种行为。

## 惊恐万状！Minecraft 服务器出大乱子了

*“而当火凤凰帝国向其他领土发动进攻时，一切都改变了。”<sup>2</sup>*

整个圈子顿时就陷入了混乱。一下子，一个平日里和谐的圈子里爆发了一场虚拟的内战。这种情况是双方都可以理解的，因为都是出于不同的原因。

每个人都可以自己开一个多人游戏服务器，所以有些恶意的服务器就有可能会诈骗玩家（一般是小孩子）的钱。这些诈骗行为通常是在付了款之后不兑换相应的物品，或者是不久后就关服了。常常会有人在受骗之后（一般是孩子父母带着怒火）就去找 Mojang 求助，而这些服务器本身就不归 Mojang 所管。

同时，也有许多没有恶意的人围绕 Minecraft 宽松的知识产权建立了商业模式（尽管这并不明智）。开源软件轻松地扩展了 Minecraft，而 Mojang 却并不想要这样的扩张。

## 第二幕：Bukkit 真相大白

商业服务器、开源软件开发者和 Mojang 三者的关系已经十分紧张。到 2014 年，Mojang 已经是一家价值数百万美元的公司了（当时甚至还没有被微软以数十亿美元收购）。EULA 的冲突给开源软件开发者带来了沉重的负担，他们承受着来自双方的压力。

然后，在 2014 年 8 月 21 日，意想不到的事情发生了。Bukkit 项目的主开发者，Warren Loo（EvilSeph），宣布 Bukkit 项目停止开发：

> @CraftBukkit: 是时候说再见了 –
> http://t.co/LRG2uiMbDe
>
> — EvilSeph (@EvilSeph) 2014 年 8 月 21 日

阅读 Bukkit 团队的[完整公告](https://web.archive.org/web/20151105173217/https://bukkit.org/threads/bukkit-its-time-to-say.305106/)

## “所有权”

这是一则坏消息，但真正惊人的消息是在一个小时之后，Mojang 的 Minecraft 主开发者在 Twitter 上作出的回应：

> Warren 停止开发 Bukkit 了，但他可能忘记了一件事：Mojang 已经在两年多前买下了这个项目。项目的停止不是由他来决定的。
> 
> — Jens Bergensten (@jeb\_) [2014 年 8 月 21 日](https://twitter.com/jeb_/status/502380018216206336?ref_src=twsrc%5Etfw)

另外两个在 Mojang 工作的 Bukkit 前开发者也附和着说：

> 我们接管了 Bukkit 的 GitHub 仓库和项目。拭目以待。
>
> — Erik Broes (@_grum) [2014 年 8 月 21 日](https://twitter.com/_grum/status/502381523241144320?ref_src=twsrc%5Etfw)


> 这件事应该说清楚：Bukkit 为 Mojang 所有。我个人会把 Bukkit 更新到 1.8，但 Bukkit 不是并且不会成为官方的 API。
>
> — Nathan Adams (@Dinnerbone) [2014 年 8 月 21 日](https://twitter.com/Dinnerbone/status/502389963606867968?ref_src=twsrc%5Etfw)

现在真相大白了：Bukkit 的开源开发者在受 Mojang 之雇后，作为雇佣合同的一部分，放弃了个人版权和他们的开源贡献者的权利。一条 140 字的推文，让开源开发者和商业服务器的圈子都了解到了这一点。

圈子里充满了不解、沮丧、愤怒：

> “收购 Bukkit 代码库的决定，原本是 Mojang 和 Curse 的一个秘密，直到最近才被曝光。我完全没意识到，Bukkit 独立运行就是个假象——亏我之前还花了两年的时间去做 Bukkit 的管理员和继承负责人。要是我早知道的话，我可能就另谋高就了，也不会在这做贡献了。一件事有什么后果，想是很容易想，但到底该怎么办，那还真得等事发了才知道。”
>
> — TnT, "[So long, and thanks for all the fish](https://web.archive.org/web/20150215082334/https://bukkit.org/threads/so-long-and-thanks-for-all-the-fish.305350/)"

现在所知的是，这个由志愿者推动了三年左右的开源项目，它的“所有权”已经落到了一家价值数百万美元的公司手里，而这家公司基本没有做过什么贡献来支持这个开源项目，这个项目却建立起了这款游戏的社区。Mojang 对 Bukkit 做出的唯一可见的贡献就是，允许他们在法律的灰色地带继续努力。

## 第三幕：Bukkit 的 DMCA 移除

2014 年 9 月 5 日，Bukkit 的一位非 Mojang 员工的、为这个项目贡献了 15000 多行代码的主开发者，对他[个人在这个项目上所做的所有贡献](https://github.com/github/dmca/blob/master/2014/2014-09-05-CraftBukkit.md)，引出了一个数字千年版权法（DMCA）移除。**一天之内，一个使用量约是 Mojang 官方服务端软件的 3 倍的项目，它的源代码消失在了互联网。**

这位主开发者为什么要这么做，很容易理解。花了人生几年时间来贡献于一个游戏项目，结果发现这个项目已经偷偷地归一家数百万美元的公司所有了，这样的经历确实让人深受打击——这基本上就是免费劳动。但是，这同时也是世界上无数人在使用的项目。这不只是一个项目，更是整个圈子。

Bukkit 的一个主开发者，在自己的[退圈信](https://web.archive.org/web/20161213172659/https://bukkit.org/threads/bukkit-an-autobiography.310083/)中，这样评价这个项目：

> “Bukkit 项目的意义远超 CraftBukkit 项目、发布更新和提供 API。它的意义是为圈子腾出一个空间，让大家能有机会，用我们的产品随心所欲地编程。Bukkit API 让人们能够改变 Minecraft 的运行方式，但如果没有圈子里的插件开发者的贡献，这就毫无意义。”
>
> — [feildmaster](https://bukkit.org/members/feildmaster.82116/)

DMCA 移除不只是移除了软件，更是撕裂了社区。Bukkit 的一夜消失导致了群龙无首的状态，仇恨、骚扰和人肉充斥在每个地方。（别忘了，这时也正是 [#GamerGate](https://en.wikipedia.org/wiki/Gamergate_controversy) 时期。）

## 谁充斥着这个圈子？

我参与的 Spigot 项目创建于 2012 年，是 Bukkit 的一个分支。Spigot 像 Bukkit 一样，也收到了 DMCA 移除，但 Spigot 团队想出了一个巧妙的法律变通方法，得以继续开发。

庞大的插件社区和第三方软件，通过 Bukkit 的 API，围绕着 Bukkit 和 Spigot 蓬勃发展。不寻常的是，除了少数例外，这些圈子的领导者大多是 20 多岁的成年人、青少年，甚至是 11 岁的孩子。在这个圈子里，开源并不是一个得到了透彻理解的概念。**只是大家都在这么做而已**。围绕许可证说太多[并不总是很好](https://www.spigotmc.org/threads/the-most-important-part-of-your-project-might-not-even-be-a-line-of-code.121682/)，但开放地工作就是这个游戏圈子运作的本质。

## 开源精神已死

对于这个圈子来说，开源的前途和荣誉已经逝去。多年以来，Bukkit 开发团队与社区有着同样的对开源的信仰：

> “Bukkit 选择让我们的 API 走上开源之路有几个原因。不只是因为开源是件好事，还是因为 Minecraft 圈里有很多有才的人，他们可以帮助我们发展、成熟、壮大我们的项目，其速度远超我们的想象。”
>
> — EvilSeph (Warren Loo), "[Bukkit Project Changes and Improvements](https://web.archive.org/web/20150308122118/https://bukkit.org/threads/bukkit-project-changes-and-improvements.133798/)"

但是，这个项目使用 GPL 已经注定了要失败，并且其所有权又如此神秘，再加上受到了 DMCA 移除，在这样的复杂情况下，开源的蓬勃发展既帮助了这个圈子，又毁灭了这个圈子。

## 谁对？谁错？

一方面，发布 DMCA 移除令的主开发者，在发现了 Mojang 的“免费劳动”协议后，可以发泄自己的不满（他付出了巨大的个人代价，因为他受到了骚扰、跟踪，还收到了死亡威胁）。另一方面，集体社区面临着一个时代的结束，在这种特殊情况下，GPL 作为一个有效的许可证，已经失去了效力：

> “许可证就是一份合同。合同失效或者从一开始就无效的原因有很多。其中一个条件就是被‘欺骗’达成协议，这包括在含有伪造的情况下同意从事某个项目。如上所述，一个开源项目被一家公司秘密收购，目的是借此改进该公司的游戏，这是你能找到的接近免费劳动的漏洞。免费劳动不久前就在这个国家禁止了。我们为此进行了一场战争”
>
> /u/VideoGameAttorney, “[My Response to Vubui, Mojang, and the hundreds (yes, hundreds) of you who asked me to weigh in on this.](https://www.reddit.com/r/Minecraft/comments/2fk5nn/my_response_to_vubui_mojang_and_the_hundreds_yes/)“

对于这个传奇故事，我唯一的结论来自于那同一篇 Reddit 主题：“但是最后，不要光相信一方是‘对的’而另一方是‘错的’。这些事情远没有那么简单。”

## 为何我撰下此文？

因为我一直会回想到这个故事，这贯穿了我的生活。[2020 年 2 月](https://blog.justinwflory.com/tag/2020-foss-conferences/)，我参加了一个 copyleft 许可证会议。我在写这场会议的活动报告的时候，向在场的人复述了这个同样的故事。这并不是我第一次在会议上讲这个故事。这是一个关于 copyleft 许可的有趣的案例研究。

因为这是在开源的游戏世界里，而且这个游戏圈子里的大部分人年龄都不过 30 岁。许多开源的“老手”都不曾知晓这个故事。

但这作为我参与的第一个开源社区，而且是我投入了人生将近十年时间的社区（因为还有其他数不清的项目），这段经历形成了我对开源和社区的独特看法，以至于我无法忘记。这段故事的结局尽管很突然，但仍然值得传诵，以向那些投入了远比我多的时间、精力、金钱和泪水的人们给予敬意。

## 译者注

1. 原文错误地将此许可证称为“GNU Public License”，本文已经改正。
2. 这段话来自电视连续剧《降世神通》（Avatar: The Last Airbender），表示情况产生了突然的剧变。

---

Copyright (C) Justin W. Flory 2020  
Copyright (C) Peaksol 2021-2022

This page is licensed under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
