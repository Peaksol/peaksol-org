# Issues with the English Version of the Mulan Public License

The [Mulan Public License, Version 2](http://license.coscl.org.cn/MulanPubL-2.0) is a *bilingual* copyleft license published by COSOL. It is approved by neither FSF nor OSI, though its sibling license, the [Mulan Permissive Software License, Version 2](http://license.coscl.org.cn/MulanPSL2) was approved by OSI as an ["open source" license](https://opensource.org/license/mulanpsl-2-0). What I'm talking about today is, however, not [license proliferation](https://en.wikipedia.org/wiki/License_proliferation) or anything like that, but rather how problematic the English version of that license can be.

According to its license terms, the Mulan Public License "is written in both Chinese and English", both "having the same legal effect", and "in case of divergence, the Chinese version shall prevail." I'm glad the English version doesn't look like it's machine-translated, but from what you're about to read soon, that could be exactly what extinguished my gladness.

Starting from Section 0 of the license, we've already got a typo here:

> **Affiliates** <u>mmeans</u> entities that control, are controlled by, or are under common control...

Moving on to Section 2, which is about grant of patent license:

> ... such patent license is only limited to the patent claims owned or controlled by such Contributor now or in future ..., <u>excluding of</u> any patent claims solely <u>be</u> infringed by your modification.

There's no "excluding of", just "excluding" (the incorrect one is used in Section 0 as well). Also, just so it's grammatical, use "to be" instead of "be".

For Section 4, we've got a lot to talk about:

> If you Distribute your Derivative Work, you <u>have to</u>:
>
> (i) accompanying the <u>Derivative work</u>, provide recipients with <u>Corresponding Source Code</u> of your Derivative Work under this License...

In order to indicate requirements, "must" should be used instead of "have to". Also, "Derivative *work*" is not capitalized, and a "the" is missing before "Corresponding Source Code".

> If you provide the Corresponding Source Code through a download link, you should place such link address prominently in the Derivative Work or its accompanying documents, and <u>be valid no less than three years</u> from your Distribution of the particular Derivative Work, and...

The whole parallel construction grammatically indicates that it is *you* rather than the link that should "be valid". To keep the subject consistent while expressing the correct object, use a causative verb. Also, a preposition is needed there for "no less than three years" to follow. Taken together, this becomes "keep it valid for no less than three years."

> (ii) accompanying the Derivative Work, provide recipients with a written offer indicating your willingness to provide the Corresponding Source Code of the Derivative Work <u>licensed</u> under this License.

I venture that the word "licensed" is redundant, though the Chinese version *does* make this part attributive rather than adverbial.

> Such written offer shall be placed prominently in the Derivative Work or its accompanying documents. <u>Without reasonable excuse</u>, the recipient shall be able to acquire the <u>Corresponding Source code</u> of the Derivative work <u>for</u> no more than three months from your receipt of a valid request, and <u>be valid</u> no less than three years from your Distribution of the particular Derivative Work.

Apart from "Corresponding Source *code*" not fully capitalized, we're again faced with confusing subjects. Since the one keeping the written offer valid is different from the subject (the recipient) here, making the verbs parallel doesn't work anyways -- use a clause or a sentence instead ("you must keep it valid for...").

For the adverbial part ("without reasonable excuse"), even if you believe in dangling participles (which are very dangerous in formal writing, not to mention this is not even a participle), saying "unless with" rather than "without" should make more sense. But since the subjects are indeed different, and there's no even a thing called "dangling preposition", use a clause ("Unless you have a reasonable excuse").

I'll finally argue that the preposition "for" should be replaced with "within", the "from" after that thus replaced with "of".

For the rest of the license, Sections 5 to 8 plus the Appendix, each of them got us something to talk about. And for the sake of readability, Sections 7 and 8 have been transformed to lowercase.

> [Your license will not be terminated, provided that] it’s your first time <u>to receive</u> a notice of termination from such Contributor pursuant to this License, and you have cured all the breaches within 30 days of <u>receipt</u> of such notice.

"Receiving" instead of "to receive". Also, add a "your" before that "receipt".

> If you combine Contribution or your Derivative Work with a work licensed under the GNU AFFERO GENERAL PUBLIC LICENSE Version 3 (hereinafter referred to as “AGPLv3”) or its subsequent versions, and according to the AGPLv3 or its subsequent versions, you have an obligation to make the combined work to be licensed under the <u>corresponding license</u>, you can license such combined work under <u>the license</u>...

"Corresponding license" is not defined, and if you take it literally, it may also be intepreted as something else other than the AGPLv3 or its subsequent versions. The same applies to the phrase "the license" that follows. In the Chinese version, however, "AGPLv3 or its subsequent versions" is explicitly used here.

> Contribution <u>are</u> provided without warranties of any kind, either express or implied. In no event shall any Contributor or copyright holder be liable to you for any damages, including, but not limited to any direct, <u>or</u> indirect, special or consequential damages arising from your use or inability to use the Contribution...

"Contribution" is not plural, as indicated elsewhere in the license as well. Also, "or" is redundant.

> <u>In the case of</u> divergence between the Chinese and English versions, the Chinese version shall prevail.

"In case of", not "in the case of", since this is not about a specific case.

> Create a file named “LICENSE” which contains the whole <u>context</u> of this License in the first directory of your software package;

I doubt if what they mean is "content", but "a copy of this License" just sounds much better.

## Conclusion

While I've certainly got a lot more to talk about, I'm already tired of writing at this point. The steward could have been more professional.

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
