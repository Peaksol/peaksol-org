# Posts

An asterisk (*) denotes that the article is a translated work that I may or may not necessarily give endorsement to.

- [Issues with the English Version of the Mulan Public License](mulanpubl.html)  
August 25, 2024

- [My Hardware](hardware.html)  
March 25, 2024

- [An Interesting Trademark Infringement Case](youlan-v-zhoukou.html)  
July 11, 2023

- [Licensing Tricks](licensing-tricks.html)  
July 11, 2023

- [GNOME 打印技巧](gnome-printing-skills.html) (GNOME Printing Skills)  
July 5, 2023

- [让代码协作平台走向联邦化*](towards-federated-forges.html) (Towards Federated Forges)  
June 22, 2023

- [Don't Say "Licensed under CC0"](cc0.html)  
December 24, 2022

- [谁害怕公有领域？*](publicdomain.html) (Who's Afraid of the Public Domain?)  
November 4, 2022

- [采访 FSF 执行董事 Zoë Kooyman*](meet-fsf-executive-director-zoe-kooyman.html) (Meet FSF Executive Director Zoë Kooyman)  
July 18, 2022

- [一段关于 Minecraft、Bukkit 和 GPL 的故事*](the-day-open-source-died.html) (A story about Minecraft, Bukkit, and the GPL)  
December 25, 2021

- [通用产品代码](upc.html) (Universal Product Code)  
November 1, 2020

- [基于半角标点符号的中文技术性文档格式化标准](text-formatting-guidelines.html) (Yet another paradigm of Chinese text formatting)  
October 1, 2020

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
