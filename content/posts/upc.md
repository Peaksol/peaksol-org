# 通用产品代码 (Universal Product Code)

## 特性

- 仅由 0 和 1 组成. 黑条为 1, 白条为 0;
- 连续的黑条和白条的长度可以为 1, 2, 3 或 4;
- 单个 UPC 由 30 条黑条组成, 其二进制长度为 95 位;
- 可双向识别 (通过检查单个数字的对应编码中的 0 或 1 的个数的奇偶来决定解码反向).

## 识别

从左到右扫描, 将第一个黑条长度定为长为 1 位的标准长度, 并对下一个白条采取相同的措施; 完成 95 位标准长度的识别.

## 校验

- UPC 左右各有长度为 3 的护线 (101), 中间有长度为 5 的护线 (01010);
- 左右各护线与中间护线包围的两个区域有奇偶校验.

## 解码

- 以左右护线 (101) 和中间护线 (01010), 将 UPC 划分为两个区域;
- 将每个区域内的数分成 6 组 7 位长的数组;
- 对于两个区域, 将包含的任意一个数组中的 1 的个数为奇数的区域定为 A 区, 另一个区域定为 B 区.
- 以从 A 区到 B 区的方向, 将 A 区和 B 区中的数组分别定义为 A1 - A6 和 B1 - B6;
- 根据下表, 解码 UPC: (解码顺序为 A1 - A6, B1 - B6; 解 A 区时, A 为 1, B 为 0; 解 B 区时, B 为 1, A 为 0; 如果 B 区在 A 区左边, 则将每个数组中的数左右翻转后再查表;)

```
BBBAABA = 0
BBAABBA = 1
BBABBAA = 2
BAAAABA = 3
BABBBAA = 4
BAABBBA = 5
BABAAAA = 6
BAAABAA = 7
BAABAAA = 8
BBBABAA = 9
```

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
