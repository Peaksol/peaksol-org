# My Hardware

Here is a list of devices that I use for my everyday computing. All devices, except those with a ~~strikethrough~~, works with 100% free software.

- Laptop: [Lenovo ThinkPad X270](https://h-node.org/notebooks/view/en/2337) with [Trisquel GNU/Linux 11](https://trisquel.info)
- WiFi Cards:
  - [Atheros QCA9565 / AR9565](https://h-node.org/wifi/view/en/1487) (M.2)
  - [Atheros AR9271](https://h-node.org/wifi/view/en/357) (USB)
- Bluetooth: [CSR8510](https://h-node.org/bluetooth/view/en/167) (USB)
- 3G/4G Card: [Huawei K3765](https://h-node.org/threegcards/view/en/490) with [Gammu/Wammu](https://wammu.eu/) (USB)
- Printer/Scanner: [Canon PIXMA MG2540S](https://h-node.org/printers/view/en/2053/)
- Cellphones: ~~[OnePlus 6](https://wiki.lineageos.org/devices/enchilada/) with [LineageOS 21](https://lineageos.org/)~~

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
