# Don't Say "Licensed under CC0"

The [CC0](https://creativecommons.org/publicdomain/zero/1.0/) Public Domain Dedication is NOT a license by its nature, though it is often mistakenly referred to as a license because it does affect the copyright to the work.

The reason for this is that a license is based on copyright law, permitting usage under specific conditions, and that the author still owns copyright on which the practice of licensing is legally based. On the other hand, public domain means an absolute waiver of all copyright to the greatest extent possible.

That being said, the only circumstance in which a CC0 dedication becomes a license is when waiver of copyright is impossible in the jurisdiction where the dedication happens. In that case, CC0 falls back to a license as permissive as possible. However, this doesn't mean CC0 is a license by its nature.

Statement of a CC0 dedication should not imply it is a copyright license. Therefore, it is incorrect to say a work is "licensed under CC0", and a copyright notice like "Copyright (C) 2022 Alice" should not be attached. Instead, it's recommended to say a work is "marked with CC0".

Additionally, for programmers who wish to place their computer software into the public domain, it is recommended to name the copy of CC0 in their codebase "COPYING" rather than "LICENSE".

## See Also
- [CC0 FAQ](https://wiki.creativecommons.org/wiki/CC0_FAQ)

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
