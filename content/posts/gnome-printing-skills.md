# GNOME 打印技巧

以下教程的原始纸张大小都是 A4. 你也可以在相应的地方做出调整, 从而使用其他大小的纸张.

## 如何打印一套试卷

下面的步骤可以实现以打印一套试卷的方式打印一份文档, 即每张纸打印连续的 4 页 (双面打印, 每面两页).

1. 对要打印的 PDF 文件, 调出 GNOME 打印对话框.
2. 在 General 选项卡中, 选择目标打印机, 并在 Range 区域指定所有页面.
3. 在 Page Setup 选项卡中:
	- 将 Pages per side 调为 `2`,
	- 将 Page ordering 调为 `Left to right`,
	- 将 Only print 调为 `Odd sheets`,
	- 将 Paper size 调为 `A4`,
	- 根据需要调节 Scale.
4. 在 Advanced 选项卡中, 根据需要调节 Print Quality 并决定是否彩印.
5. 点击 Print, 并等待打印完毕.
6. **上下**翻转打印好的一沓纸, 使其背面朝上. 接着, 将这沓纸放入打印机的进纸口. (不要改变纸张顺序.)
7. 重复一次步骤 1-5, 但在第 3 步中将 Only print 调为 `Even sheets`.

注: 若需确定配置是否无误, 可在步骤 1 中选择 Print to file 并预览打印效果.

## 如何打印一本小册子 (骑马钉装订)

骑马钉装订是指, 将一沓纸张对折, 并在折痕处打订书订, 使其成为一本小册子.

因为文档内的所有页面需要以特定方式重新排列, 并且 GNOME 的打印窗口似乎没办法直接这么做, 所以我写了一个 Python 3 脚本来实现它:

```python
import PyPDF2
import math

reader = PyPDF2.PdfReader(open('input.pdf', 'rb'))
pages = len(reader.pages)
pieces = math.ceil(pages / 4)

front_writer = PyPDF2.PdfWriter()
back_writer = PyPDF2.PdfWriter()

def get_page(index):
	if(index >= pages):
		return PyPDF2.PageObject.create_blank_page(reader)
	return reader.pages[index]

for i in range(pieces):
	front_writer.add_page(get_page(pieces * 4 - i * 2 - 1));
	front_writer.add_page(get_page(i * 2));
	back_writer.add_page(get_page(i * 2 + 1));
	back_writer.add_page(get_page(pieces * 4 - i * 2 - 2));

front_writer.write(open('output_front.pdf', 'wb'))
back_writer.write(open('output_back.pdf', 'wb'))
print("Done. Pieces of paper needed: %s" % pieces)
```

运行脚本, 会将 `input.pdf` 转换为 `output_front.pdf` 和 `output_back.pdf`, 并输出所需纸张数. 确保纸张和墨水足够后, 开始打印.

1. 对 `output_front.pdf` 文件, 进行一次上一章节的步骤 1-5, 但在第 3 步中将 Only print 调为 `All sheets`.
2. **上下**翻转打印好的一沓纸, 使其背面朝上. 接着, 将这沓纸放入打印机的进纸口. (不要改变纸张顺序.)
3. 对 `output_back.pdf` 文件, 进行一次上一章节的步骤 1-5, 但在第 3 步中将 Only print 调为 `All sheets`.

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
