# Licensing Tricks

Projects that claim to be "open source" (I would say free software, though) but are "source-available", i.e. non-free (linked are **last commits** where I found such licensing tricks applied):

1. **[mirai](https://github.com/mamoe/mirai/tree/2d83b69), a protocol library and chat bot framework for QQ.**  
Licensed under AGPL with additional terms saying "no commercial use".

2. **[QNotified](https://github.com/ferredoxin/QNotified/tree/1b7b0ab), tweaks for the Android QQ client.**  
Licensed under AGPL with additional EULA saying "no commercial use".

3. **[PCL](https://github.com/Hex-Dragon/PCL2/tree/24cb459), a third-party Minecraft launcher.**  
Not corresponding source code: only part of it is released; not giving 4 essential freedoms: "study but do not modify".

4. **[Pikapika](https://github.com/niuhuan/pikapika/tree/ed36d2e), a third-party client for PicACG.**  
Licensed under the MIT (Expat) License with additional terms in README saying "no commercial use" and "don't share it however you wish".

5. **[SmsForwarder](https://github.com/pppscn/SmsForwarder/tree/1fe9362), an Android app forwarding imcoming SMSes elsewhere.**  
Licensed under the 2-Clause BSD License with additional terms in README saying "no commercial use".

It is appreciated that terms to prohibit non-free software hosting have been introduced by some code hosting services including [Codeberg](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md) and [NotABug](https://notabug.org/tos), excluding GitHub.

---

To the extent possible under law, the person who associated [CC0](http://creativecommons.org/publicdomain/zero/1.0) with this work has waived all copyright and related or neighboring rights to this work.
