<?php
/*
 * This file is released into the public domain, marked with CC0.
 * A copy of the CC0 Public Domain Dedication can be found on:
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

$is_index = ($basename == 'index.md');
if ($is_index) {
	register_dir($dir_relative, $frontmatter);
}
$parent_frontmatters = get_parent_frontmatters($dir_relative);

[$main, $footer] = split($parsed);
?>
<!DOCTYPE html>
<head>
	<title><?php
		$titles = [];
		if(!$is_index) array_push($titles, $frontmatter['title']);
		foreach (array_reverse($parent_frontmatters) as $parent_frontmatter) {
			array_push($titles, $parent_frontmatter['title']);
		}
		echo implode(' - ', $titles);
	?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/css/main.css" rel="stylesheet">
	<?php if (isset($frontmatter['style'])) { ?>
		<link href="/css/<?= $frontmatter['style'] ?>" rel="stylesheet">
	<?php } ?>
</head>

<body>
	<nav>
		<div><?php
			foreach ($parent_frontmatters as $parent_dir => $parent_frontmatter) { ?>
				<a href="<?= $parent_dir . '/' ?>"><?= $parent_frontmatter['title']; ?></a>
				<span> / </span><?php
			} ?>
		</div>
		<div class="links"><?php
			if (isset($frontmatter['nav'])) {
				foreach ($frontmatter['nav'] as $item) { ?>
					<a href="<?= $item['link'] ?>"><?= $item['title'] ?></a><?php
				}
			}?>
		</div>
	</nav>

	<main>
		<?= $main ?>
	</main>

	<?php if ($footer) { ?>
		<footer>
			<?= $footer ?>
		</footer>
	<?php } ?>
</body>
