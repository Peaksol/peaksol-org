<?php
/*
 * This file is released into the public domain, marked with CC0.
 * A copy of the CC0 Public Domain Dedication can be found on:
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

$frontmatters = [];

function register_dir($dir, $frontmatter) {
	global $frontmatters;
	$frontmatters[$dir] = $frontmatter;
}

function get_parent_frontmatters($dir) {
	global $frontmatters;
	$parent_frontmatters = [];
	$levels = explode('/', $dir);
	for($cursor = 0; $cursor < count($levels); $cursor ++) {
		$parent_dir = implode('/', array_slice($levels, 0, $cursor + 1));
		$parent_frontmatters[$parent_dir] = $frontmatters[$parent_dir];
	}
	return $parent_frontmatters;
}

function split($parsed) {
	$hrtag = '<hr />';
	$hrpos = strrpos($parsed, $hrtag);
	if ($hrpos !== false) {
		return [
			substr($parsed, 0, $hrpos),
			substr($parsed, $hrpos + strlen($hrtag))
		];
	}
	return [$parsed, ''];
}
